package com.tecsup.petclinic.web;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.tecsup.petclinic.domain.Owner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class OwnerControllerTest {

	private static final ObjectMapper om = new ObjectMapper();
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testGetOwners() throws Exception {

		//int NRO_RECORD = 73;
		int ID_FIRST_RECORD = 1;

		this.mockMvc.perform(get("/owners"))
					.andExpect(status().isOk())
					.andExpect(content()
					.contentType(MediaType.APPLICATION_JSON_UTF8))
		//		    .andExpect(jsonPath("$", hasSize(NRO_RECORD)))
					.andExpect(jsonPath("$[0].id", is(ID_FIRST_RECORD)));
	}
	
	@Test
	public void testFindOwnerOK() throws Exception {

		String FIRST_NAME = "David";
		String LAST_NAME = "Schroeder";
		String CITY = "Madison";

		mockMvc.perform(get("/owner/9"))  // Object must be BASIL 
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				//.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(9)))
				.andExpect(jsonPath("$.firstName", is(FIRST_NAME)))
				.andExpect(jsonPath("$.lastName", is(LAST_NAME)))
				.andExpect(jsonPath("$.city", is(CITY)));
				//.andExpect(jsonPath("$.birthDate", is("2000-09-07")));
				//.andExpect(jsonPath("$.birthDate", is(new SimpleDateFormat("yyyy-MM-dd").format(DATE))));

	}
	
	@Test
	public void testFindOwnerKO() throws Exception {

		mockMvc.perform(get("/owner/666"))
				.andExpect(status().isNotFound());

	}
	
	@Test
    public void testCreateOwner() throws Exception {
		
		String FIRST_NAME = "Miguel";
		String LAST_NAME = "Inga";
		String CITY = "Lima";
		String ADDRESS = "Av. Los arandanos";
		String TELEPHONE = "123456789";
		
		Owner newOwner = new Owner(FIRST_NAME, LAST_NAME, CITY, ADDRESS,TELEPHONE);
	
	    mockMvc.perform(post("/owner")
	            .content(om.writeValueAsString(newOwner))
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
	            .andDo(print())
	            .andExpect(status().isCreated())
	            //.andExpect(jsonPath("$.id", is(1)))
	            .andExpect(jsonPath("$.firstName", is(FIRST_NAME)))
	            .andExpect(jsonPath("$.lastName", is(LAST_NAME)))
	            .andExpect(jsonPath("$.city", is(ADDRESS)))
	    		.andExpect(jsonPath("$.address", is(CITY)))
	    		.andExpect(jsonPath("$.telephone", is(TELEPHONE)));
    
	}
	/*
	@Test
    public void testDeleteOwner() throws Exception {

		String FIRST_NAME = "Ale";
		String LAST_NAME = "Inga";
		String CITY = "Lima";
		String ADDRESS = "Av. Los arandanos";
		String TELEPHONE = "123456789";
		
		Owner newOwner = new Owner(FIRST_NAME, LAST_NAME, CITY, ADDRESS,TELEPHONE);
		
		ResultActions mvcActions = mockMvc.perform(post("/newOwner")
	            .content(om.writeValueAsString(newOwner))
	            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
	            .andDo(print())
	            .andExpect(status().isCreated());
	            
		String response = mvcActions.andReturn().getResponse().getContentAsString();

		Integer id = JsonPath.parse(response).read("$.id");

        mockMvc.perform(delete("/owner/" + id ))
                /*.andDo(print())*/
               /* .andExpect(status().isOk());
    }*/
	
}















